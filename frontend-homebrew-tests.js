// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by frontend-core.js.
import { name as packageName } from "meteor/frontend-core";

// Write your tests here!
// Here is an example.
Tinytest.add('frontend-core - example', function (test) {
  test.equal(packageName, "frontend-core");
});
