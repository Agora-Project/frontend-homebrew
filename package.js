Package.describe({
  name: 'agoraforum:frontend-homebrew',
  version: '0.0.3',
  // Brief, one-line summary of the package.
  summary: 'Homebrew frontend for the Agora forum software.',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/Agora-Project/frontend-homebrew/',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: null
  //documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.8.0.2');

    api.use([
        'iron:router@1.1.2',
        'accounts-password',
        'useraccounts:core@1.14.2',
        'useraccounts:iron-routing@1.14.2',
        'utilities:avatar',
        'alanning:roles@1.2.16',
        'blaze-html-templates@1.1.2'
    ]);

    api.use([
        'ecmascript',
        'templating@1.3.2',
        'session',
        'meteorhacks:subs-manager@1.6.4',
        'liquidautumn:useraccounts-mdl@0.99.2',
        'zodiase:mdl@1.3.0',
        'utilities:avatar@0.9.2',
    ], 'client');

    api.addFiles([
        'client/lib/notifier.js',
        'client/lib/requestAnimationFrame.js',
        'client/lib/templateParents.js',
        'client/lib/seenPosts.js',
        'client/lib/layeredGrapher.js',
        'client/lib/identityCollections.js',

        'client/subscriptionManager.js',

        'client/main.html',
        'client/main.css',
        'client/init.js',

        'client/userList/userList.html',
        'client/userList/userList.js',

        'client/federation/federation.html',
        'client/federation/federation.js',
        'client/federation/federation.css',

        'client/errorPage/errorPage.html',

        'client/mainView/detailed/detailed.html',
        'client/mainView/detailed/detailed.css',
        'client/mainView/detailed/detailed.js',

        'client/mainView/reply/reply.html',
        'client/mainView/reply/reply.css',
        'client/mainView/reply/reply.js',

        'client/mainView/main.html',
        'client/mainView/main.css',

        'client/mainView/layout.js',
        'client/mainView/partitioner.js',
        'client/mainView/camera.js',
        'client/mainView/renderer.js',
        'client/mainView/main.js',

        'client/userProfile/userProfile.html',
        'client/userProfile/userProfile.css',
        'client/userProfile/userProfile.js',

        'client/adminScreen/adminScreen.html',
        'client/adminScreen/adminScreen.css',
        'client/adminScreen/adminScreen.js',

        'client/routes.js'
    ], 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('agoraforum:frontend-homebrew');
  api.mainModule('frontend-homebrew-tests.js');
});
